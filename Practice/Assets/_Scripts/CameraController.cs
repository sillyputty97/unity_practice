﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	public GameObject Glob;

	Vector3 offset;

	// Use this for initialization
	void Start () {

		offset = transform.position - Glob.transform.position;
		
	}
	
	// Update is called once per frame
	void LateUpdate () {
		transform.position = Glob.transform.position + offset;
	}
}
